#include <stdio.h>  // Usando: printf(), scanf()
#include <stdlib.h> // Usando: malloc(), free()
#include <string.h> // Usando: strcmp()

struct no{ // Define como é um nó da pilha
    char dado;
    struct no *anterior; // Ponteiro para o nó anterior
}typedef NO;

typedef NO* PILHA; // Define o tipo PILHA. O qual é um ponteiro para o nó que está no topo da pilha

// Cria uma pilha
PILHA* criarPilha(){
    PILHA* topoPilha = (PILHA*)malloc(sizeof(PILHA));
    if(topoPilha != NULL){ // Se foi alocado com sucesso
        *topoPilha = NULL; // deixe o conteúdo vazio
    }
    return topoPilha;
}

// Empilha e guarda dadoOrigem na pilha
int push(PILHA* topoPilha, char dadoOrigem){
    if(topoPilha == NULL) return 0; // Se não existe, erro

    NO* elemento = (NO*)malloc(sizeof(NO));
    if(elemento == NULL) return 0; // Se não foi possível alocar, erro

    elemento->dado = dadoOrigem; // Guarda informação dentro do nó
    elemento->anterior = *topoPilha; // elemento → [topoPilha = no_1] → no_2 ...
    *topoPilha = elemento; // Elemento vira topo da pilha: [topoPilha = elemento] → no_1 → no_2 ...
    
    return 1;
}

// Desempilha, resgata o conteúdo guardado e insere em dadoDestino
int pop(PILHA* topoPilha, char* dadoDestino){
    if(topoPilha == NULL || *topoPilha == NULL) return 0; // Se não existe ou topoPilha tem conteúdo vazio, retorna erro

    *dadoDestino = (*topoPilha)->dado;  // Preenche o dadoDestino
    NO *noAuxiliar = *topoPilha; // Guarda antigo topo da pilha
    *topoPilha = (*topoPilha)->anterior; // Topo da pilha passa a ser o elemento anterior

    free(noAuxiliar); // Libera antigo topo da pilha

    return 1;
}

// Converte char em letra minúscula
char toLowerCase(char txt){
    if(txt >= 65 && txt <= 90) txt += 32;
    return txt;
}

// Verifica se é um palíndromo
int verficarPalindromo(char palavra[]){
    PILHA* pilha = criarPilha();

    for(int i=0; i < palavra[i]!='\0'; i++){ // Empilha palavra recebida
        palavra[i] = toLowerCase(palavra[i]); // Passa a palavra para forma minúscula
        push(pilha, palavra[i]);
    }

    int tamanho = strlen(palavra); // Tamanho da palavra
    char palavraReversa[tamanho];
    for(int i=0; i < tamanho; i++){ // Desempilha
        pop(pilha, &palavraReversa[i]);
    }
    palavraReversa[tamanho] = '\0'; // Adiciona indicador de final
    free(pilha); // Libera pilha da memória

    if(strcmp(palavra, palavraReversa) == 0){ // Verifica se a palavra é igual a palavraReversa
        return 1;
    }else{
        return 0;
    }
}

int main(){
    char txt[30] = "\0";

    printf(
        "\x1B[1;32m======== Palíndromo ==========\x1b[0m\n"
        "Insira uma palavra: "
    );
    scanf("%[^\n]", txt);

    if(verficarPalindromo(txt)){
        printf("A palavra \x1b[1;32m%s\x1b[0m é um palíndromo.\n\n", txt);
    }else{
        printf("A palavra \x1b[1;31m%s\x1b[0m \x1b[1;4mnão\x1b[0m é um palíndromo.\n\n", txt);
    }

    return 0;
}
