# Variávies
CC = gcc
CFLAGS = -pedantic-errors -Wall

# Regra : dependências
all: palindromo.c
	$(CC) $(CFLAGS) palindromo.c -o palindromo

clean:
	rm palindromo

debug:
	make CFLAGS+=-g
